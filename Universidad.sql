CREATE DATABASE [Universidad]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'library', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER02\MSSQL\DATA\Universidad.mdf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'library_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER02\MSSQL\DATA\Universidad_log.ldf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 WITH LEDGER = OFF
GO
USE [Universidad]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'PRIMARY') ALTER DATABASE [library] MODIFY FILEGROUP [PRIMARY] DEFAULT
GO

-- ************************* Tabla de Candidatos ************************* --
CREATE TABLE [dbo].[Candidatos] (
	id_candidato INT NOT NULL IDENTITY (1,1),
	nombre_candidato VARCHAR (180) NOT NULL,
	aplellido_candidato VARCHAR (180) NOT NULL,
	procedencia_candidato VARCHAR (100) NOT NULL,
	id_carrera INT NOT NULL,
	contacto_candidato VARCHAR (12) NOT NULL,
	documento_candidato VARCHAR (12) NULL,
	id_estadoCandidato INT NOT NULL,
	PRIMARY KEY (id_candidato)
);
GO
SELECT * FROM [dbo].[Candidatos];
GO
-- UPDATE [dbo].[Candidatos] SET id_estadoCandidato = 1 WHERE id_candidato = 2;


-- ************************* Tabla de Candidatos ************************* --
CREATE TABLE [dbo].[EstadoCandidato] (
	id_estadoCandidato INT NOT NULL IDENTITY (1,1),
	estado_candidato VARCHAR (100) NOT NULL,
	PRIMARY KEY (id_estadoCandidato)
);
GO
SELECT * FROM [dbo].[EstadoCandidato];
GO

-- ************************* Tabla de Carreas ************************* --
CREATE TABLE [dbo].[Carreras] (
	id_carrera INT NOT NULL IDENTITY (1,1),
	nombre_carrera VARCHAR (100) NOT NULL,
	codigo_carrera VARCHAR (50) NOT NULL,
	id_facultad INT NOT NULL,
	id_tipoCarrera INT NOT NULL,
	PRIMARY KEY (id_carrera)
);
GO
SELECT * FROM [dbo].[Carreras];
GO

-- ************************* Tabla de Carreas ************************* --
CREATE TABLE [dbo].[TipoCarrera] (
	id_tipoCarrera INT NOT NULL IDENTITY (1,1),
	tipoCarrera VARCHAR (100) NOT NULL,
	PRIMARY KEY (id_tipoCarrera)
);
GO
SELECT * FROM [dbo].[TipoCarrera];
GO

-- ************************* Tabla de Carreas ************************* --
CREATE TABLE [dbo].[Facultad] (
	id_facultad INT NOT NULL IDENTITY (1,1),
	nombre_facultad VARCHAR (150) NOT NULL,
	encargado_faculad VARCHAR (150) NOT NULL,
	PRIMARY KEY (id_facultad)
);
GO
SELECT * FROM [dbo].[Facultad];
GO

-- ************************* Tabla de Estudiantes ************************* --
CREATE TABLE [dbo].[Estudiantes] (
	carnet_estudiante INT NOT NULL IDENTITY (2023000, 1),
	nombre_estudiante VARCHAR (150) NOT NULL,
	apellido_estudiante VARCHAR (150) NOT NULL,
	documento_estudiante VARCHAR (10) NULL,
	procedencia_estudiante VARCHAR (250) NOT NULL,
	contacto_estudiante VARCHAR (9) NOT NULL,
	INE_estudiante INT NULL,
	id_estadoEstudiante INT NOT NULL,
	id_carrera INT NOT NULL 
	PRIMARY KEY (carnet_estudiante)
);
GO
SELECT * FROM [dbo].[Estudiantes];

-- ************************* Tabla de Estudiantes ************************* --
CREATE TABLE [dbo].[EstadoEstudiantes] (
	id_estadoEstudiante INT NOT NULL IDENTITY(1,1),
	estadoEstudiante VARCHAR (100) NOT NULL,
	PRIMARY KEY (id_estadoEstudiante)
);
GO

-- ************************* Tabla de Estudiantes ************************* --
CREATE TABLE [dbo].[Matriculas] (
	id_matricula INT NOT NULL IDENTITY (1,1),
	id_asignatura INT NOT NULL,
	id_estudiante INT NOT NULL,
	id_carrera INT NOT NULL,
	fecha_matricula DATE
	PRIMARY KEY (id_matricula)
);
GO
SELECT * FROM [dbo].[Matriculas];
GO

-- ************************* Tabla de Registros ************************* --
CREATE TABLE [dbo].[Registros] (
	id_registro INT NOT NULL IDENTITY (1,1),
	id_estudiante INT NOT NULL,
	id_carrera INT NOT NULL,
	pagos FLOAT,
	fecha_registro DATE,
	PRIMARY KEY (id_registro)
);
GO
SELECT * FROM [dbo].[Registros];

-- ************************* Tabla de Estudiantes ************************* --
CREATE TABLE [dbo].[Asignatura] (
	id_asignatura INT NOT NULL IDENTITY (1,1),
	nombre_asignatura VARCHAR (150) NOT NULL,
	catedratico_asignatura VARCHAR (150) NOT NULL,
	codigo_asignatura VARCHAR (10) NOT NULL,
	id_estadoAsignatura INT NOT NULL,
	PRIMARY KEY (id_asignatura)
);
GO
-- Codigos de asignaturas: 100(Ingenier�as) 200 (Diplomados) 300(Licenciaturas) 400(Ciencias B�sicas)
SELECT * FROM [dbo].[Asignatura];
GO

-- ************************* Tabla de Notas ************************* --
CREATE TABLE [dbo].[Notas] (
	id_nota INT NOT NULL IDENTITY (1,1),
	id_asignatura INT NOT NULL,
	id_estudiante INT NOT NULL,
	nota_obtenida FLOAT
	PRIMARY KEY (id_nota)
);
GO
SELECT * FROM [dbo].[Notas];

-- ************************* Tabla de Estado de Asignaturas ************************* --
CREATE TABLE [dbo].[EstadoAsignatura] (
	id_estadoAsignatura INT NOT NULL IDENTITY (1,1),
	estado_Asignatura VARCHAR (100) NOT NULL,
	PRIMARY KEY (id_estadoAsignatura)
);
GO
SELECT * FROM [dbo].[EstadoAsignatura];
GO

-- ************************* Tabla Historial del Estudiante ************************* --
CREATE TABLE [dbo].[BitacoraEstudiantil] (
	id_bitacora INT NOT NULL IDENTITY (1,1),
	id_asignatura INT NOT NULL,
	id_estadoAsignaturaInscrita INT NOT NULL,
	id_estudiante INT NOT NULL,
	numero_matricula INT NOT NULL,
	PRIMARY KEY (id_bitacora)
);
GO
SELECT * FROM [dbo].[BitacoraEstudiantil];
GO
-- UPDATE [dbo].[BitacoraEstudiantil] SET id_estadoAsignaturaInscrita = 2 WHERE id_bitacora = 1;

-- ************************* Tabla de Estado de Asignaturas ************************* --
CREATE TABLE [dbo].[EstadoAsignaturaInscrita] (
	id_asignaturaInscrita INT NOT NULL IDENTITY (1,1),
	estado_asignatura VARCHAR (100) NOT NULL,
	PRIMARY KEY (id_asignaturaInscrita)
);
GO
SELECT * FROM [dbo].[EstadoAsignaturaInscrita];
GO

-- * --------------------------------- Uniones de las tablas --------------------------------- * --

-- ********** Tabla de Candidato -> Estado Candidatos **********
ALTER TABLE [dbo].[Candidatos] ADD CONSTRAINT FK_estadoCandidato
FOREIGN KEY (id_estadoCandidato) REFERENCES [dbo].[EstadoCandidato] (id_estadoCandidato)

-- ********** Tabla de Candidatos -> Carreras **********
ALTER TABLE [dbo].[Candidatos] ADD CONSTRAINT FK_carreras
FOREIGN KEY (id_carrera) REFERENCES [dbo].[Carreras] (id_carrera) 

-- ********** Tabla de Carreras -> Facultad **********
ALTER TABLE [dbo].[Carreras] ADD CONSTRAINT FK_facultad
FOREIGN KEY (id_facultad) REFERENCES [dbo].[Facultad] (id_facultad)

-- ********** Tabla de Carreras -> Facultad **********
ALTER TABLE [dbo].[Registros] ADD CONSTRAINT FK_registro
FOREIGN KEY (id_carrera) REFERENCES [dbo].[Carreras] (id_carrera)

-- ********** Tabla de Registro -> Estudiante **********
ALTER TABLE [dbo].[Registros] ADD CONSTRAINT FK_estudiante
FOREIGN KEY (id_estudiante) REFERENCES [dbo].[Estudiantes] (carnet_estudiante)

-- ********** Tabla de Matriculas -> Estudiante **********
ALTER TABLE [dbo].[Matriculas] ADD CONSTRAINT FK_matricula
FOREIGN KEY (id_estudiante) REFERENCES [dbo].[Estudiantes] (carnet_estudiante)

-- ********** Tabla de Matriculas -> Estudiante **********
ALTER TABLE [dbo].[Matriculas] ADD CONSTRAINT FK_asignatura
FOREIGN KEY (id_asignatura) REFERENCES [dbo].[Asignatura] (id_asignatura)

-- ********** Tabla de Estudiante -> EstadoEstudiante **********
ALTER TABLE [dbo].[Estudiantes] ADD CONSTRAINT FK_estadoEstudiante
FOREIGN KEY (id_estadoEstudiante) REFERENCES [dbo].[EstadoEstudiantes] (id_estadoEstudiante)

-- ********** Asignatura -> EstadoAsignatura **********
ALTER TABLE [dbo].[Asignatura] ADD CONSTRAINT FK_estadoAsignatura
FOREIGN KEY (id_estadoAsignatura) REFERENCES [dbo].[EstadoAsignatura] (id_estadoAsignatura)

-- ********** Asignatura -> EstadoAsignatura **********
ALTER TABLE [dbo].[Carreras] ADD CONSTRAINT FK_tipoCarrera
FOREIGN KEY (id_tipoCarrera) REFERENCES [dbo].[TipoCarrera] (id_tipoCarrera)

-- ********** Bitacora -> Estudiantes **********
ALTER TABLE [dbo].[BitacoraEstudiantil] ADD CONSTRAINT FK_bitacora
FOREIGN KEY (id_estudiante) REFERENCES [dbo].[Estudiantes] (carnet_estudiante)

-- ********** Bitacora -> Estado Asignatura Inscrita **********
ALTER TABLE [dbo].[BitacoraEstudiantil] ADD CONSTRAINT FK_estadoAsignaturaInscita
FOREIGN KEY (id_estadoAsignaturaInscrita) REFERENCES [dbo].[estadoAsignaturaInscrita] (id_asignaturaInscrita)

-- ********** Asignatura -> Notas **********
ALTER TABLE [dbo].[Notas] ADD CONSTRAINT FK_notas
FOREIGN KEY (id_asignatura) REFERENCES [dbo].[Asignatura] (id_asignatura)

-- ****************************** Insercciones a la base de datos ******************************

-- Insert de la tabla de Estado de los Estudiantes
INSERT INTO [dbo].[EstadoEstudiantes] (estadoEstudiante) VALUES ('Activo');
INSERT INTO [dbo].[EstadoEstudiantes] (estadoEstudiante) VALUES ('Inactivo');
INSERT INTO [dbo].[EstadoEstudiantes] (estadoEstudiante) VALUES ('Condicional');
INSERT INTO [dbo].[EstadoEstudiantes] (estadoEstudiante) VALUES ('Proceso de graduaci�n');

--Inserts de la tabla de Estados de los candidatos
INSERT INTO [dbo].[EstadoCandidato] (estado_candidato) VALUES ('Aprobado');
INSERT INTO [dbo].[EstadoCandidato] (estado_candidato) VALUES ('Denegado');

--Inserts de la tabla Tipo de Carreras
INSERT INTO [dbo].[TipoCarrera] (tipoCarrera) VALUES ('Ingenier�as');
INSERT INTO [dbo].[TipoCarrera] (tipoCarrera) VALUES ('Licenciaturas');
INSERT INTO [dbo].[TipoCarrera] (tipoCarrera) VALUES ('T�cnicos');
INSERT INTO [dbo].[TipoCarrera] (tipoCarrera) VALUES ('Diplomados');

--Inserts de la tabla de Facultades
INSERT INTO [dbo].[Facultad] (nombre_facultad, encargado_faculad) VALUES ('Ingenier�as', 'Luis Alonso Cabrera');
INSERT INTO [dbo].[Facultad] (nombre_facultad, encargado_faculad) VALUES ('Ciencias Econ�micas', 'Ana Luisa Deras');
INSERT INTO [dbo].[Facultad] (nombre_facultad, encargado_faculad) VALUES ('Ciencias y Humanidades', 'Juan Carlos Grande');
INSERT INTO [dbo].[Facultad] (nombre_facultad, encargado_faculad) VALUES ('Facultad de Aeron�utica', 'Nestor Enrrique Salamanca');
INSERT INTO [dbo].[Facultad] (nombre_facultad, encargado_faculad) VALUES ('Ciencias B�sicas', 'Nestor Enrrique Salamanca');

-- Inserts de la tabla de Estado de las asignaturas que han sido Inscritas
INSERT INTO [dbo].[EstadoAsignaturaInscrita] (estado_asignatura) VALUES ('Reprobada');
INSERT INTO [dbo].[EstadoAsignaturaInscrita] (estado_asignatura) VALUES ('Aprobada');

--Inserts de la tabla de Estado de las asignaturas
INSERT INTO [dbo].[EstadoAsignatura] (estado_Asignatura) VALUES ('Activa');
INSERT INTO [dbo].[EstadoAsignatura] (estado_Asignatura) VALUES ('Inactiva');

--Inserts de la tabla de asignaturas
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('C�lculo Diferencial', 'Luis Alonso Arenivar', 'ALG501', 1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Programaci�n Estructurada', 'Juan Carlos Aguilar', 'PRE101', 1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Programaci�n Orientada a Objetos', 'David Enersto Perez', 'POO101' , 1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Dibujo t�cnico 1', 'Pricila de Guevara Cruz', 'DT-102',1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Qu�mica General', 'Juan Obvidio de Preza', 'QMG501' ,1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('T�cnica Fotogr�fica', 'Miguel Angel de la O', 'FT302',1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Cinem�tica y din�mica de particulas', 'Ingrid Ester Menjivar', 'CDP501',1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Redes Cisco CCNA', 'Edgardo Daniel Hernandez', 'RCN101',1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Gesti�n de Recusos', 'Anderson Rogel de Preza', 'GR301', 1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Avi�nica', 'Felipe Martinez', 'AV103',1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Mantenimiento de sistemas Hidr�ulicos', 'Jos� Miguel Estrada', 'MTH102', 1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Din�mica de fluidos viscosos y no viscosos', 'Milton Estrada', 'DFV103',1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('T�cnicas de locuci�n verbal', 'Mirna de Martinez', 'TLV203',1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Ingles para el trabajo corporativo', 'Issela Guevara Porras','ITP203', 1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('T�cnicas de Mantenimiento', 'Juan Carlos Aguilar', 'TMA103',1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Animaci�n 2D y 3D', 'Amilcar Amadeo Cruz', 'AD302',1);
INSERT INTO [dbo].[Asignatura] (nombre_asignatura, catedratico_asignatura,codigo_asignatura ,id_estadoAsignatura) VALUES ('Estadistica', 'Nestor Montes Parras', 'EST501',1);

--Inserts de la tabla carreras
INSERT INTO [DBO].[Carreras] (nombre_carrera, codigo_carrera, id_facultad, id_tipoCarrera) VALUES ('Ingenier�a en Ciencias de la Computaci�n', '101', 1, 1);
INSERT INTO [DBO].[Carreras] (nombre_carrera, codigo_carrera, id_facultad, id_tipoCarrera) VALUES ('Ingenier�a Industrial', '102', 1, 1);
INSERT INTO [DBO].[Carreras] (nombre_carrera, codigo_carrera, id_facultad, id_tipoCarrera) VALUES ('Ingenier�a Aeron�utica','103', 3, 1);
INSERT INTO [DBO].[Carreras] (nombre_carrera, codigo_carrera, id_facultad, id_tipoCarrera) VALUES ('Diplomado en Ciencias de la comunicaci�n', '201', 3, 4);
INSERT INTO [DBO].[Carreras] (nombre_carrera, codigo_carrera, id_facultad, id_tipoCarrera) VALUES ('Diplomado en Hoteles y turismo', '202', 2, 4);
INSERT INTO [DBO].[Carreras] (nombre_carrera, codigo_carrera, id_facultad, id_tipoCarrera) VALUES ('Diplomado en Idiomas', '203', 3, 4);
INSERT INTO [DBO].[Carreras] (nombre_carrera, codigo_carrera, id_facultad, id_tipoCarrera) VALUES ('Licenciatura en Administraci�n de Empresas', '301', 2, 2);
INSERT INTO [DBO].[Carreras] (nombre_carrera, codigo_carrera, id_facultad, id_tipoCarrera) VALUES ('Licenciatura en Dise�o gr�fico', '302', 3, 2);
INSERT INTO [DBO].[Carreras] (nombre_carrera, codigo_carrera, id_facultad, id_tipoCarrera) VALUES ('Licenciatura en Telecomunicaciones', '303', 3, 2);

-- Insercciones de la tabla de Candidatos
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Nelson Jos�', 'Almendares Ruiz', 'Instituto T�cnico Ricaldone', 1, '7375-7645', '01234512-2', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Ana Luc�a', 'Vasquez Torres', 'Colegio Santa M�nica', 4, '9834-3143', '05381234-2', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Jos� Amilcar', 'Chac�n Gonzales', 'Colegio la Diveina Providencia', 5, '0424-3452', '09423452-7', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Diana Berenice', 'Aguilar Pe�a', 'Instituto T�cnico Ricaldone', 2, '4014-4512', '04912314-3', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Milton Daniel', 'Hecheverr�a Cruz', 'Colegio Santa Cecilia', 3, '2344-6516', '54910312-9', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Ana Mirna', 'Monje', 'Instituto Cant�n De la Paz', 7, '8034-2341', '03225234-3', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Hector Enmanuel', 'Garcia Luna', 'Colegio T�cnico de San Roque', 3, '0923-1233', '34983123-8', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Wilfredo Alejandro', 'Villeda', 'Colegio EXAL', 6, '2344-1235', '30131232-5', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Victor Esteban', 'Perez de la O', 'Instituto T�cnico Ricaldone', 1, '89340932', '00200314-3', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Joel Alberto', 'Enrriquez Enrriquez', 'Colegio Militar Salvadore�o', 2, '9034-3461', '09423421-1', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Eriberto Steve', 'Edmons', 'Instituto T�cnico Ricaldone', 1, '8923-1234', '44123123-0', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Jos� Daniel', 'Rodas Cer�n', 'Instituto T�cnico Ricaldone', 5, '4216-1451', '23454512-4', 2);
INSERT INTO [dbo].[Candidatos] (nombre_candidato, aplellido_candidato, procedencia_candidato, id_carrera, contacto_candidato, documento_candidato, id_estadoCandidato)
	VALUES ('Carlos Alberto', 'Ponce Ramirez', 'Colegio Divina Providencia', 4, '9083-3234', '42342344-3', 2);
GO

--INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (2, 2023000, 1, GETDATE());

-- ****************************** Vista para los detallos de los candidatos ******************************
CREATE VIEW View_Candidatos 
	AS SELECT nombre_candidato AS Nombre, aplellido_candidato AS Apellido, procedencia_candidato AS Procedencia, nombre_carrera AS Carrera, contacto_candidato AS Contacto, estado_candidato
	FROM [dbo].[Candidatos]
	INNER JOIN [dbo].[Carreras] ON [dbo].[Carreras].id_carrera = [dbo].[Candidatos].id_carrera
	INNER JOIN [dbo].[EstadoCandidato] ON [dbo].[Candidatos].id_estadoCandidato = [dbo].[EstadoCandidato].id_estadoCandidato
GO
SELECT * FROM View_Candidatos;
GO

-- ******************************  Trigger para agregar los candidatos que fueron aceptados a la tabla de Estudiantes ****************************** 
CREATE TRIGGER AgregarEstudiantes
ON [dbo].[Candidatos] 
AFTER UPDATE
AS
BEGIN
	IF UPDATE (id_estadoCandidato)
	BEGIN 
		INSERT INTO [dbo].[Estudiantes] (nombre_estudiante, apellido_estudiante, documento_estudiante, contacto_estudiante, procedencia_estudiante, id_carrera, id_estadoEstudiante)
		SELECT i.nombre_candidato, i.aplellido_candidato, i.documento_candidato, i.contacto_candidato, i.procedencia_candidato , i.id_carrera, 1
		FROM inserted i
		WHERE id_estadoCandidato = 1
	END;
END;
GO

-- ****************************** Trigger para registrar las notas y materias que fueron inscritas por el alumno en un ciclo ****************************** 
CREATE TRIGGER AgregarBitacora
ON [dbo].[Matriculas]
AFTER INSERT
AS
BEGIN
	INSERT INTO [dbo].[BitacoraEstudiantil] (id_asignatura, id_estudiante, id_estadoAsignaturaInscrita, numero_matricula)
	SELECT id_asignatura, id_estudiante, 1, 1
	FROM inserted

	INSERT INTO [dbo].[Notas] (id_asignatura, id_estudiante, nota_obtenida)
	SELECT id_asignatura, id_estudiante, 0
	FROM inserted
END
GO

-- ******************************  Procedimiento para validar los registros de un Estudiante ****************************** 
CREATE PROCEDURE CheckDataRegister (@carnet INT, @carrera_id INT)
AS
BEGIN
	IF @carrera_id = 1 AND EXISTS (SELECT 1 FROM [dbo].[Registros] WHERE id_estudiante = @carnet) OR 
		@carrera_id = 2 AND EXISTS (SELECT 1 FROM [dbo].[Registros] WHERE id_estudiante = @carnet) OR 
		 @carrera_id = 3 AND EXISTS (SELECT 1 FROM [dbo].[Registros] WHERE id_estudiante = @carnet)
	BEGIN
		PRINT 'El estudiante ya pertenece a una carrera'
	END
	-- En el caso de que no coincida con los primeros datos evaluamos si es un diplomado y si no se ha inscrito en m�s de 2 de los mismos 
	ELSE 
	BEGIN
		DECLARE @max INT; SET @max = 2;

		IF @carrera_id = 4 AND (SELECT COUNT (*) AS Cantidad FROM [dbo].[Registros] WHERE id_estudiante = @carnet) >= @max OR
			@carrera_id = 5 AND (SELECT COUNT (*) AS Cantidad FROM [dbo].[Registros] WHERE id_estudiante = @carnet) >= @max OR
			 @carrera_id = 6 AND (SELECT COUNT (*) AS Cantidad FROM [dbo].[Registros] WHERE id_estudiante = @carnet) >= @max
		BEGIN
			PRINT 'El estudiante ya lleva dos diplomados o una carrera y un diplomado.'
		END
		ELSE
			BEGIN
				PRINT 'Registro realizado correctamente.'
				-- Insertar el registro
				INSERT INTO [dbo].[Registros] (id_estudiante, id_carrera, pagos, fecha_registro)
				VALUES (@carnet, @carrera_id, 115.99, GETDATE())
			END
		END
	END
GO
EXEC CheckDataRegister @carnet = 2023000, @carrera_id = 1;

-- Consulta para ver los estudiantes regitrados
SELECT id_registro AS ID, nombre_carrera AS Carrera, CONCAT(nombre_estudiante, ' ', apellido_estudiante) Estudiante, tipoCarrera AS Departamento
	FROM [dbo].[Registros]
	INNER JOIN [dbo].[Carreras] ON [dbo].[Carreras].id_carrera = [dbo].[Registros].id_carrera
	INNER JOIN [dbo].[Estudiantes] ON [dbo].[Estudiantes].carnet_estudiante = [dbo].[Registros].id_estudiante
	INNER JOIN [dbo].[TipoCarrera] ON [dbo].[TipoCarrera].id_tipoCarrera = [dbo].[Carreras].id_tipoCarrera
	WHERE id_estudiante = 2023000

-- Consulta para ver las materias y el programa al cual est� matriculado seg�n su carnet
SELECT carnet_estudiante AS Carnet, CONCAT (nombre_estudiante, ' ' , apellido_estudiante) AS Estudiante, nombre_asignatura AS Asignatura, nombre_carrera AS Carrera
	FROM [dbo].[Matriculas]
	INNER JOIN [dbo].[Asignatura] ON [dbo].[Asignatura].id_asignatura = [dbo].[Matriculas].id_asignatura
	INNER JOIN [dbo].[Estudiantes] ON [dbo].[Estudiantes].carnet_estudiante = [dbo].[Matriculas].id_estudiante
	INNER JOIN [dbo].[Carreras] ON [dbo].[Carreras].id_carrera = [dbo].[Estudiantes].id_carrera
	WHERE carnet_estudiante = 2023000

-- Consulta dada una asignatura ver los alumnos inscritos
SELECT carnet_estudiante AS Carnet,nombre_estudiante ,nombre_asignatura AS Asignatura
	FROM [dbo].[Matriculas]
	INNER JOIN [dbo].[Asignatura] ON [dbo].[Asignatura].id_asignatura = [dbo].[Matriculas].id_asignatura
	INNER JOIN [dbo].[Estudiantes] ON [dbo].[Estudiantes].carnet_estudiante = [dbo].[Matriculas].id_estudiante
	INNER JOIN [dbo].[Carreras] ON [dbo].[Carreras].id_carrera = [dbo].[Estudiantes].id_carrera
	WHERE nombre_asignatura LIKE '%Qu�mica General%'
GO

-- Historial Academico
SELECT id_bitacora, carnet_estudiante AS Carnet,nombre_asignatura AS Asignatura, CONCAT(nombre_estudiante, ' ', apellido_estudiante) AS Estudiante, estado_asignatura AS Estado, numero_matricula AS Matricula
FROM [dbo].[BitacoraEstudiantil]
INNER JOIN [dbo].[Asignatura] ON [dbo].[Asignatura].id_asignatura = [dbo].[BitacoraEstudiantil].id_asignatura
INNER JOIN [dbo].[Estudiantes] ON [dbo].[Estudiantes].carnet_estudiante = [dbo].[BitacoraEstudiantil].id_estudiante
INNER JOIN [dbo].[EstadoAsignaturaInscrita] ON [dbo].[EstadoAsignaturaInscrita].id_asignaturaInscrita = [dbo].[BitacoraEstudiantil].id_estadoAsignaturaInscrita
WHERE carnet_estudiante = 2023000

-- Asignaturas de una carrea
SELECT nombre_asignatura
FROM [dbo].[Asignatura]
WHERE codigo_asignatura LIKE '%101%'

SELECT * FROM [dbo].[Asignatura]
SELECT * FROM [dbo].[Carreras]


INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (2, 2023000, 1, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (4, 2023001, 4, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (14, 2023005, 3, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (3, 2023003, 5, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (2, 2023000, 1, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (5, 2023001, 4, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (16, 2023005, 3, GETDATE())
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (5, 2023000, 1, GETDATE());;
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (9, 2023001, 4, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (9, 2023003, 5, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (8, 2023000, 1, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (1, 2023003, 5, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (17, 2023001, 4, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (7, 2023003, 5, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (6, 2023005, 3, GETDATE());
INSERT INTO [dbo].[Matriculas] (id_asignatura, id_estudiante, id_carrera, fecha_matricula) VALUES (6, 2023004, 5, GETDATE());